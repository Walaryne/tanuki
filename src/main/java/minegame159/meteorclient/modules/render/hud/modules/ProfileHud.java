package minegame159.meteorclient.modules.render.hud.modules;

import minegame159.meteorclient.MeteorClient;
import minegame159.meteorclient.modules.render.hud.HUD;
import minegame159.meteorclient.systems.System;
import minegame159.meteorclient.systems.Systems;
import net.minecraft.nbt.CompoundTag;

import java.io.File;

public class ProfileHud extends DoubleTextHudModule {

    public ProfileHud(HUD hud) {
        super(hud, "profile", "Displays the last loaded profile.", "Profile: ");
    }

    protected static String lastLoadedProfile;

    public static ProfileHudSavable get() { return Systems.get(ProfileHudSavable.class); }

    @Override
    protected String getRight() {
        if(lastLoadedProfile == null) {
            get().load();
            if(lastLoadedProfile == null) {
                return "None";
            }
        }

        return lastLoadedProfile;
    }

    public static void setLastLoadedProfile(String profile) {
        lastLoadedProfile = profile;
    }

    public static class ProfileHudSavable extends System<ProfileHudSavable> {

        public ProfileHudSavable() {
            super(null);
        }

        @Override
        public File getFile() {
            return new File(new File(MeteorClient.FOLDER, "persistent"), "profile_hud.nbt");
        }

        @Override
        public CompoundTag toTag() {
            CompoundTag tag = new CompoundTag();

            if(lastLoadedProfile == null) {
                tag.putString("lastProfile", "None");
                return tag;
            }

            tag.putString("lastProfile", lastLoadedProfile);

            return tag;
        }

        @Override
        public ProfileHudSavable fromTag(CompoundTag tag) {
            lastLoadedProfile = tag.getString("lastProfile");

            return this;
        }
    }
}
