/*
 * This file is part of the Meteor Client distribution (https://github.com/MeteorDevelopment/meteor-client/).
 * Copyright (c) 2021 Meteor Development.
 */

package minegame159.meteorclient.modules.render.hud.modules;

import minegame159.meteorclient.modules.render.hud.HUD;
import net.minecraft.item.ItemStack;

public class DurabilityHud extends DoubleTextHudModule {
    public DurabilityHud(HUD hud) {
        super(hud, "durability", "Displays durability of the item you are holding.", "Durability: ");
    }

    @Override
    protected String getRight() {
        if (mc.player == null) return "";

        ItemStack st = mc.player.getMainHandStack();
        if (!st.isEmpty() && st.isDamageable()) {
            int i = st.getDamage();
            int max = st.getMaxDamage();
            int durability = max - i;
            return durability + " / " + max + " (" + ((int) Math.ceil(((double) durability / (double) max) * 100)) + "%)";
        }

        return "";
    }
}
