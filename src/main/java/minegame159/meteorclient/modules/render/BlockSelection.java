/*
 * This file is part of the Meteor Client distribution (https://github.com/MeteorDevelopment/meteor-client/).
 * Copyright (c) 2021 Meteor Development.
 */

package minegame159.meteorclient.modules.render;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.render.RenderEvent;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.rendering.DrawMode;
import minegame159.meteorclient.rendering.MeshBuilder;
import minegame159.meteorclient.rendering.Renderer;
import minegame159.meteorclient.rendering.ShapeMode;
import minegame159.meteorclient.settings.*;
import minegame159.meteorclient.utils.render.color.SettingColor;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;

public class BlockSelection extends Module {
    private final SettingGroup sgGeneral = settings.getDefaultGroup();
    private final SettingGroup sgMeteor = settings.createGroup("Meteor");
    private final SettingGroup sgTanuki = settings.createGroup("Tanuki");

    public enum Mode {
        Meteor,
        Tanuki
    }

    private final Setting<Mode> mode = sgGeneral.add(new EnumSetting.Builder<Mode>()
            .name("mode")
            .description("What rendering mode should Block Selection use.")
            .defaultValue(Mode.Tanuki)
            .build()
    );

    private final Setting<Boolean> advanced = sgMeteor.add(new BoolSetting.Builder()
            .name("advanced")
            .description("Shows a more advanced outline on different types of shape blocks.")
            .defaultValue(true)
            .build()
    );

    private final Setting<ShapeMode> shapeMode = sgMeteor.add(new EnumSetting.Builder<ShapeMode>()
            .name("shape-mode")
            .description("How the shapes are rendered.")
            .defaultValue(ShapeMode.Lines)
            .build()
    );

    private final Setting<Boolean> noDepth = sgMeteor.add(new BoolSetting.Builder()
            .name("no-depth")
            .description("Disables the depth buffering on the drawn lines/faces.")
            .defaultValue(true)
            .build()
    );

    private final Setting<SettingColor> sideColor = sgMeteor.add(new ColorSetting.Builder()
            .name("side-color")
            .description("The side color.")
            .defaultValue(new SettingColor(255, 255, 255, 50))
            .build()
    );

    private final Setting<SettingColor> meteorLineColor = sgMeteor.add(new ColorSetting.Builder()
            .name("meteor-line-color")
            .description("The line color for Meteor mode.")
            .defaultValue(new SettingColor(255, 255, 255, 255))
            .build()
    );

    private final Setting<SettingColor> tanukiLineColor = sgTanuki.add(new ColorSetting.Builder()
            .name("tanuki-line-color")
            .description("The line color Tanuki mode.")
            .defaultValue(new SettingColor(255, 255, 255, 255))
            .build()
    );

    public BlockSelection() {
        super(Category.Render, "block-selection", "Modifies how your block selection is rendered.");
    }

    private static final MeshBuilder MB;
    private static final MeshBuilder _MB;

    static {
        MB = new MeshBuilder(1024);
        _MB = new MeshBuilder(1024);
    }

    @EventHandler
    private void onRender(RenderEvent event) {
        if (mode.get() != Mode.Meteor) return;
        if (mc.crosshairTarget == null || !(mc.crosshairTarget instanceof BlockHitResult)) return;

        BlockPos pos = ((BlockHitResult) mc.crosshairTarget).getBlockPos();
        BlockState state = mc.world.getBlockState(pos);
        VoxelShape shape = state.getOutlineShape(mc.world, pos);

        if (shape.isEmpty()) return;
        Box box = shape.getBoundingBox();

        if (advanced.get()) {
            for (Box b : shape.getBoundingBoxes()) {
                render(pos, b, event);
            }
        } else {
            render(pos, box, event);
        }
    }

    private void render(BlockPos pos, Box box, RenderEvent event) {
        MB.depthTest = !noDepth.get();
        _MB.depthTest = !noDepth.get();
        MB.begin(event, DrawMode.Triangles, VertexFormats.POSITION_COLOR);
        _MB.begin(event, DrawMode.Lines, VertexFormats.POSITION_COLOR);

        Renderer.boxWithLines(MB, _MB, pos.getX() + box.minX, pos.getY() + box.minY, pos.getZ() + box.minZ, pos.getX() + box.maxX, pos.getY() + box.maxY, pos.getZ() + box.maxZ, sideColor.get(), meteorLineColor.get(), shapeMode.get(), 0);

        MB.end();
        _MB.end();
    }

    public Mode getMode() {
        return mode.get();
    }

    public Vec3d getColors() { return getDoubleVectorColor(tanukiLineColor); }

    public double getAlpha() { return (double) tanukiLineColor.get().a / 255; }

    private Vec3d getDoubleVectorColor(Setting<SettingColor> colorSetting) {
        return new Vec3d((double) colorSetting.get().r / 255, (double) colorSetting.get().g / 255, (double) colorSetting.get().b / 255);
    }
}
