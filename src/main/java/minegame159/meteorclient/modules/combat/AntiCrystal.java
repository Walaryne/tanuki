package minegame159.meteorclient.modules.combat;

import com.google.common.collect.Streams;
import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.settings.DoubleSetting;
import minegame159.meteorclient.settings.EnumSetting;
import minegame159.meteorclient.settings.Setting;
import minegame159.meteorclient.settings.SettingGroup;
import minegame159.meteorclient.utils.player.PlayerUtils;
import net.minecraft.entity.decoration.EndCrystalEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;

import java.util.Comparator;
import java.util.Optional;

public class AntiCrystal extends Module {

    public enum Mode {
        PressurePlate,
        Button
    }

    private final SettingGroup sgGeneral = settings.getDefaultGroup();

    private final Setting<Mode> mode = sgGeneral.add(new EnumSetting.Builder<Mode>()
            .name("mode")
            .description("The mode at which AntiCrystal operates.")
            .defaultValue(Mode.PressurePlate)
            .build()
    );

    private final Setting<Double> range = sgGeneral.add(new DoubleSetting.Builder()
            .name("range")
            .description("The range to place Pressure Plates/Buttons.")
            .min(1)
            .max(10)
            .defaultValue(1)
            .build()
    );

    public AntiCrystal() {
        super(Category.Combat, "AntiCrystal", "Stops End Crystals from doing damage to you.");
    }

    private int buttonSlot = -1;
    private int pressurePlateSlot = -1;

    @EventHandler
    private void onTick(TickEvent.Post event) {
        assert mc.world != null;
        assert mc.player != null;

        Optional<EndCrystalEntity> crystalTarget = Streams.stream(mc.world.getEntities())
                .filter(e -> e instanceof EndCrystalEntity)
                .filter(e -> e.distanceTo(mc.player) <= range.get() * 2)
                .filter(e -> mc.world.getBlockState(e.getBlockPos()).isAir())
                .min(Comparator.comparingDouble(o -> o.distanceTo(mc.player)))
                .map(e -> (EndCrystalEntity) e);

        crystalTarget.ifPresent(crystal -> {
            findSlots();

            if (mode.get() == Mode.PressurePlate) {
                if (pressurePlateSlot == -1) {
                    return;
                }

                PlayerUtils.placeBlock(crystal.getBlockPos(), pressurePlateSlot, Hand.MAIN_HAND);
            } else if (mode.get() == Mode.Button) {
                if (buttonSlot == -1) {
                    return;
                }

                PlayerUtils.placeBlock(crystal.getBlockPos(), buttonSlot, Hand.MAIN_HAND);
            }
        });

    }

    private void findSlots() {
        assert mc.player != null;
        buttonSlot = -1;
        pressurePlateSlot = -1;
        for (int i = 0; i < 9; i++) {
            Item it = mc.player.inventory.getStack(i).getItem();
            if (it == Items.ACACIA_BUTTON ||
                    it == Items.BIRCH_BUTTON ||
                    it == Items.CRIMSON_BUTTON ||
                    it == Items.DARK_OAK_BUTTON ||
                    it == Items.JUNGLE_BUTTON ||
                    it == Items.OAK_BUTTON ||
                    it == Items.POLISHED_BLACKSTONE_BUTTON ||
                    it == Items.SPRUCE_BUTTON ||
                    it == Items.STONE_BUTTON ||
                    it == Items.WARPED_BUTTON) {
                buttonSlot = i;
            } else if (it == Items.ACACIA_PRESSURE_PLATE ||
                    it == Items.BIRCH_PRESSURE_PLATE ||
                    it == Items.CRIMSON_PRESSURE_PLATE ||
                    it == Items.DARK_OAK_PRESSURE_PLATE ||
                    it == Items.JUNGLE_PRESSURE_PLATE ||
                    it == Items.OAK_PRESSURE_PLATE ||
                    it == Items.POLISHED_BLACKSTONE_PRESSURE_PLATE ||
                    it == Items.SPRUCE_PRESSURE_PLATE ||
                    it == Items.STONE_PRESSURE_PLATE ||
                    it == Items.WARPED_PRESSURE_PLATE) {
                pressurePlateSlot = i;
            }
        }
    }
}
