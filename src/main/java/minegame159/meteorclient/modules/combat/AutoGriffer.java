package minegame159.meteorclient.modules.combat;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.friends.Friends;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.settings.DoubleSetting;
import minegame159.meteorclient.settings.Setting;
import minegame159.meteorclient.settings.SettingGroup;
import minegame159.meteorclient.utils.entity.FakePlayerEntity;
import minegame159.meteorclient.utils.entity.FakePlayerUtils;
import minegame159.meteorclient.utils.player.InvUtils;
import minegame159.meteorclient.utils.player.Rotations;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;


public class AutoGriffer extends Module {

    private final SettingGroup sgPlace = settings.createGroup("Place");

    public AutoGriffer() {
        super(Category.Combat, "AutoGriffer", "places lava on people");
    }

    private final Setting<Double> range = sgPlace.add(new DoubleSetting.Builder()
            .name("range")
            .description("How far away the target can be to be affected.")
            .defaultValue(4)
            .min(0)
            .build()
    );

    private PlayerEntity target;
    private int stage;

    @Override
    public void onActivate() {
        target = null;
        stage = 1;
    }

    @EventHandler
    private void onTick(TickEvent.Pre event) {

        if (target != null && (mc.player.distanceTo(target) > range.get() || !target.isAlive())) target = null;

        for (PlayerEntity player : mc.world.getPlayers()) {
            if (player == mc.player || !Friends.get().attack(player) || !player.isAlive() || mc.player.distanceTo(player) > range.get())
                continue;

            if (target == null) target = player;
            else if (mc.player.distanceTo(target) > mc.player.distanceTo(player)) target = player;
        }

        if (target == null) {
            for (FakePlayerEntity player : FakePlayerUtils.getPlayers().keySet()) {
                if (!Friends.get().attack(player) || !player.isAlive() || mc.player.distanceTo(player) > range.get())
                    continue;

                if (target == null) target = player;
                else if (mc.player.distanceTo(target) > mc.player.distanceTo(player)) target = player;
            }
        }
        if (target == null) {
            stage = 1;
        }
        if (target != null) {
            if (mc.player.canSee(target)){
                switch (stage) {
                    case 1:
                        int lava = InvUtils.findItemInHotbar(Items.LAVA_BUCKET);
                        if (lava == -1) {
                            toggle();
                            return;
                        } else {
                            BlockPos anvil1 = target.getBlockPos().add(0, 1.3, 0);
                            mc.player.pitch = (float) Rotations.getPitch(anvil1);
                            mc.player.yaw = (float) Rotations.getYaw(anvil1);
                            mc.player.inventory.selectedSlot = lava;
                        }
                        stage++;
                        break;
                    case 2:
                        mc.interactionManager.interactItem(mc.player, mc.world, Hand.MAIN_HAND);
                        toggle();
                        break;
                }
            }else{
                toggle();
            }
        }
    }


    @Override
    public String getInfoString () {
        if (target != null && target instanceof PlayerEntity) return target.getEntityName();
        if (target != null) return target.getType().getName().getString();
        return null;
    }

}