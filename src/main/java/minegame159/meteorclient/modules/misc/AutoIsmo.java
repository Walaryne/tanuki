package minegame159.meteorclient.modules.misc;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.settings.*;

public class AutoIsmo extends Module {
    private final SettingGroup sgGeneral = settings.getDefaultGroup();

    private final Setting<String> playerName = sgGeneral.add(new StringSetting.Builder()
            .name("text")
            .description("The target's name.")
            .defaultValue("ismoDev")
            .build()
    );

    public AutoIsmo() {
        super(Category.Misc, "auto-ismo", "Fakes totem pops in chat, lol ismoDev moment.");
    }

    private int timer;
    private int count;
    private boolean setTimer;

    @Override
    public void onActivate() {
        count = 1;
        mc.player.sendChatMessage(playerName.get() + " has popped " + count + " totem!");
    }


    @EventHandler
    private void onTick(TickEvent.Pre event) {
        if (setTimer){
            timer = (int)(Math.random() * 70 + 40);
            setTimer = false;
        }
        timer--;
        if(timer < 0){
            count++;
            mc.player.sendChatMessage(playerName.get() + " has popped " + count + " totems!");
            setTimer = true;
        }
    }
}