//
//Made by SpicyRedPotato (BIG COCK)
//
package minegame159.meteorclient.modules.misc;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.utils.player.ChatUtils;
import minegame159.meteorclient.waypoints.Waypoint;
import minegame159.meteorclient.waypoints.Waypoints;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.ChestBlockEntity;
import net.minecraft.util.math.BlockPos;


public class EgapFinder extends Module {

    private boolean check;
    private boolean lock;
    private int stage = 0;
    private int checkDelay;
    private int comparatorHold = 0;
    private int counter = 0;
    private BlockPos chest;
    private BlockPos prevChest;
    private Waypoint waypoint;

    public EgapFinder() {
        super(Category.Misc, "EgapFinder", "Finds Egaps in a SP world and waypoints them.");
    }

    @Override
    public void onActivate() {
        stage = 0;
        checkDelay = 0;
        lock = true;
        ChatUtils.moduleError(this, "STARTING");
    }

    @Override
    public void onDeactivate() {
        ChatUtils.moduleError(this, "STOPPING");
    }

    @EventHandler
    private void onTick(TickEvent.Pre event) {
        check = false;
        for (BlockEntity blockEntity : mc.world.blockEntities) {
            if (blockEntity instanceof ChestBlockEntity) {
                if (blockEntity.isRemoved()) continue;
                chest = blockEntity.getPos();

                check = true;
                lock = false;
            }
        }
        if(!check){
            checkDelay++;
        }else{
            checkDelay = 0;
        }
        if(checkDelay >= 2){
            lock = true;
            checkDelay = 0;
            stage = 1;
        }

        if (!lock) {
            if (stage == 0) {
                stage = 1;
            }
            switch (stage) {
                case 1: {
                    ChatUtils.moduleInfo(this, "CASE 1");
                    int adjacent = chest.getX() - 1;
                    Block block = mc.world.getBlockState(chest.add(-1, 0, 0)).getBlock();
                    if (block != Blocks.COMPARATOR) {
                        mc.player.sendChatMessage("/setblock " + adjacent + " " + chest.getY() + " " + chest.getZ() + " minecraft:comparator[facing=east]");
                    }
                    stage++;
                    break;
                }
                case 2: {
                    ChatUtils.moduleInfo(this, "CASE 2");
                    int xAdjacent = chest.getX() - 1;
                    int yAdjacent = chest.getY() + 1;
                    Block block = mc.world.getBlockState(chest.add(-1, +1, 0)).getBlock();
                    if (block != Blocks.ACACIA_LEAVES) {
                        mc.player.sendChatMessage("/setblock " + xAdjacent + " " + yAdjacent + " " + chest.getZ() + " minecraft:acacia_leaves");
                    }
                    comparatorHold++;
                    if (comparatorHold == 3) {
                        stage++;
                        comparatorHold = 0;
                    }
                    break;
                }
                case 3: {
                    ChatUtils.moduleInfo(this, "CASE 3");
                    Block block = mc.world.getBlockState(chest).getBlock();
                    if (block == Blocks.CHEST) {
                        mc.player.sendChatMessage("/execute if data block " + chest.getX() + " " + chest.getY() + " " + chest.getZ() + " Items[{id:\"minecraft:enchanted_golden_apple\"}] as @p run setblock " + chest.getX() + " " + chest.getY() + " " + chest.getZ() + " minecraft:diamond_block");
                    }
                    prevChest = chest;
                    stage++;
                    break;
                }
                case 4: {
                    ChatUtils.moduleInfo(this, "CASE 4");
                    Block diamondPos = mc.world.getBlockState(prevChest).getBlock();
                    if (diamondPos == Blocks.DIAMOND_BLOCK) {
                        ChatUtils.moduleError(this, "EGAP FOUND");
                        counter++;

                        waypoint = new Waypoint();
                        waypoint.name = "EGAP" + counter;
                        waypoint.x = prevChest.getX();
                        waypoint.y = prevChest.getY();
                        waypoint.z = prevChest.getZ();
                        waypoint.maxVisibleDistance = Integer.MAX_VALUE;
                        waypoint.overworld = true;

                        Waypoints.get().add(waypoint);
                    } else {
                        mc.player.sendChatMessage("/setblock " + chest.getX() + " " + chest.getY() + " " + chest.getZ() + " minecraft:air");
                    }
                    mc.world.blockEntities.clear();
                    stage++;
                    break;
                }
            }
            if(stage == 5){
                stage = 1;
            }
        }
    }
}