package minegame159.meteorclient.modules.misc;

import baritone.api.BaritoneAPI;
import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.settings.DoubleSetting;
import minegame159.meteorclient.settings.Setting;
import minegame159.meteorclient.settings.SettingGroup;
import minegame159.meteorclient.utils.player.ChatUtils;

public class PauseOnUnloaded extends Module {

    private final SettingGroup sgGeneral = settings.getDefaultGroup();

    private final Setting<Double> readahead = sgGeneral.add(new DoubleSetting.Builder()
            .name("Readahead")
            .description("How far the module should 'look ahead' for unloaded chunks.")
            .min(1)
            .max(40)
            .sliderMin(1)
            .sliderMax(40)
            .defaultValue(12)
            .build()
    );

    public PauseOnUnloaded() {
        super(Category.Misc, "PauseOnUnloaded", "Pauses Baritone when attempting to enter an unloaded chunk.");
    }

    private boolean paused;
    private int pausedChunkX;
    private int pausedChunkZ;

    @Override
    public void onActivate() {
        paused = false;
    }

    @EventHandler
    private void onTick(TickEvent.Pre event) {

        int chunkX = (int) ((mc.player.getX() + (mc.player.getVelocity().getX() * readahead.get())) / 32);
        int chunkZ = (int) ((mc.player.getZ() + (mc.player.getVelocity().getZ() * readahead.get())) / 32);

        if (!mc.world.getChunkManager().isChunkLoaded(chunkX, chunkZ) && BaritoneAPI.getProvider().getPrimaryBaritone().getPathingBehavior().isPathing() && !paused) {
            BaritoneAPI.getProvider().getPrimaryBaritone().getCommandManager().execute("pause");
            ChatUtils.moduleInfo(this, "Entering unloaded chunk, pausing Baritone.");
            paused = true;
            pausedChunkX = chunkX;
            pausedChunkZ = chunkZ;
        } else if(paused && mc.world.getChunkManager().isChunkLoaded(pausedChunkX, pausedChunkZ)) {
            BaritoneAPI.getProvider().getPrimaryBaritone().getCommandManager().execute("resume");
            ChatUtils.moduleInfo(this, "Chunk was loaded, resuming Baritone.");
            paused = false;
        }
    }
}
