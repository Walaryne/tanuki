package minegame159.meteorclient.modules.player;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.entity.player.DropItemEvent;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.settings.ItemListSetting;
import minegame159.meteorclient.settings.Setting;
import minegame159.meteorclient.settings.SettingGroup;
import net.minecraft.item.Item;

import java.util.ArrayList;
import java.util.List;

public class AntiDrop extends Module {
    private final SettingGroup sgGeneral = settings.getDefaultGroup();

    private final Setting<List<Item>> items = sgGeneral.add(new ItemListSetting.Builder()
            .name("items")
            .description("The items to prevent from being dropped.")
            .defaultValue(new ArrayList<>())
            .build()
    );

    public AntiDrop() {
        super(Category.Player, "AntiDrop", "Stops you from dropping specified items with Q.");
    }

    @EventHandler
    private void onDropItem(DropItemEvent dropItemEvent) {
        int hotbarSlot = mc.player.inventory.selectedSlot;

        Item item = mc.player.inventory.getStack(hotbarSlot).getItem();

        if (items.get().contains(item)) {
            dropItemEvent.cancel();
        }
    }

}
