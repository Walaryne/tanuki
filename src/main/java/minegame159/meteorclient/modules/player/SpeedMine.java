/*
 * This file is part of the Meteor Client distribution (https://github.com/MeteorDevelopment/meteor-client/).
 * Copyright (c) 2021 Meteor Development.
 */

package minegame159.meteorclient.modules.player;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.entity.player.BreakBlockEvent;
import minegame159.meteorclient.events.entity.player.StartBreakingBlockEvent;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.mixin.StatusEffectInstanceAccessor;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.settings.DoubleSetting;
import minegame159.meteorclient.settings.EnumSetting;
import minegame159.meteorclient.settings.Setting;
import minegame159.meteorclient.settings.SettingGroup;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.network.packet.c2s.play.PlayerActionC2SPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import static net.minecraft.entity.effect.StatusEffects.HASTE;

public class SpeedMine extends Module {

    public enum Mode {
        Normal,
        Haste1,
        Haste2,
        Instant
    }

    private final SettingGroup sgGeneral = settings.getDefaultGroup();
    private final SettingGroup sgInstantMine = settings.createGroup("Instant Mine");

    public final Setting<Mode> mode = sgGeneral.add(new EnumSetting.Builder<Mode>()
            .name("mode")
            .defaultValue(Mode.Normal)
            .build()
    );

    public final Setting<Double> modifier = sgGeneral.add(new DoubleSetting.Builder()
            .name("modifier")
            .description("Mining speed modifier. An additional value of 0.2 is equivalent to one haste level (1.2 = haste 1).")
            .defaultValue(1.4D)
            .min(0D)
            .sliderMin(1D)
            .sliderMax(10D)
            .build()
    );

    public final Setting<Double> instantMineDelay = sgInstantMine.add(new DoubleSetting.Builder()
            .name("instant-mine-delay")
            .description("The delay for instant mine mode.")
            .defaultValue(0)
            .min(0)
            .sliderMax(20)
            .build()
    );

    public SpeedMine() {
        super(Category.Player, "speed-mine", "Allows you to quickly mine blocks.");
    }

    private int ticks;
    private final BlockPos.Mutable blockPos = new BlockPos.Mutable(0, 0, 0);
    private Direction direction;
    private boolean shouldInstantMine;

    @Override
    public void onActivate() {
        ticks = 0;
        blockPos.set(0, 0, 0);
        shouldInstantMine = false;
    }

    @EventHandler
    private void onStartBreakingBlock(StartBreakingBlockEvent event) {
        direction = event.direction;
        blockPos.set(event.blockPos);
        shouldInstantMine = false;
    }

    @EventHandler
    private void onBlockBroken(BreakBlockEvent event) {
        if(event.blockPos.equals(blockPos)) {
            shouldInstantMine = true;
        }
    }

    @EventHandler
    public void onTick(TickEvent.Post event) {
        Mode mode = this.mode.get();
        ticks++;

        if (mode == Mode.Haste1 || mode == Mode.Haste2) {
            int amplifier = mode == Mode.Haste2 ? 1 : 0;
            if (mc.player.hasStatusEffect(HASTE)) {
                StatusEffectInstance effect = mc.player.getStatusEffect(HASTE);
                ((StatusEffectInstanceAccessor) effect).setAmplifier(amplifier);
                if (effect.getDuration() < 20) {
                    ((StatusEffectInstanceAccessor) effect).setDuration(20);
                }
            } else {
                mc.player.addStatusEffect(new StatusEffectInstance(HASTE, 20, amplifier, false, false, false));
            }
        } else if (mode == Mode.Instant) {
            if(ticks >= instantMineDelay.get()) {
                if(shouldInstantMine) {
                    mc.getNetworkHandler().sendPacket(new PlayerActionC2SPacket(PlayerActionC2SPacket.Action.STOP_DESTROY_BLOCK, blockPos, direction));
                }
                ticks = 0;
            }
        }
    }
}
