/*
 * This file is part of the Meteor Client distribution (https://github.com/MeteorDevelopment/meteor-client/).
 * Copyright (c) 2020 Meteor Development.
 */

package minegame159.meteorclient.modules.player;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.world.CollisionShapeEvent;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import net.minecraft.block.Material;
import net.minecraft.util.shape.VoxelShapes;

public class AntiFire extends Module {
    public AntiFire() {
        super(Category.Player, "anti-fire", "Prevents you from walking into fire.");
    }

    @EventHandler
    public void onCollisionShape(CollisionShapeEvent e) {
        if (e.state.getMaterial() == Material.FIRE) {
            e.shape = VoxelShapes.fullCube();
        }
    }
}
