package minegame159.meteorclient.modules.movement;

import meteordevelopment.orbit.EventHandler;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.mixininterface.IVec3d;
import minegame159.meteorclient.modules.Category;
import minegame159.meteorclient.modules.Module;
import minegame159.meteorclient.settings.*;
import minegame159.meteorclient.utils.player.ChatUtils;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class TanukiMode extends Module {

    private final SettingGroup sgGeneral = settings.getDefaultGroup();

    private final Setting<Double> activationWindow = sgGeneral.add(new DoubleSetting.Builder()
            .name("activation-window")
            .description("The area above the target Y level at which pull activates.")
            .min(0.20)
            .max(5.00)
            .sliderMin(0.20)
            .sliderMax(5.00)
            .defaultValue(0.5)
            .build()
    );

    private final Setting<Integer> driftToHeight = sgGeneral.add(new IntSetting.Builder()
            .name("drift-to-height")
            .description("Y level to find blocks to drift onto.")
            .min(0)
            .max(256)
            .sliderMin(0)
            .sliderMax(256)
            .defaultValue(5)
            .build()
    );

    private final Setting<Double> horizontalPullStrength = sgGeneral.add(new DoubleSetting.Builder()
            .name("horizontal-pull")
            .description("The horizontal speed/strength at which you drift to the goal block.")
            .min(0.1)
            .max(10)
            .sliderMin(0.1)
            .sliderMax(10)
            .defaultValue(1)
            .build()
    );

    private final Setting<Double> verticalPullStrength = sgGeneral.add(new DoubleSetting.Builder()
            .name("vertical-pull")
            .description("The vertical speed/strength at which you drift to the goal block.")
            .min(0.1)
            .max(10)
            .sliderMin(0.1)
            .sliderMax(10)
            .defaultValue(1)
            .build()
    );

    private final Setting<Integer> searchRadius = sgGeneral.add(new IntSetting.Builder()
            .name("search-radius")
            .description("The radius at which tanuki mode searches for blocks (odd numbers only).")
            .min(3)
            .max(15)
            .sliderMin(3)
            .sliderMax(15)
            .defaultValue(3)
            .build()
    );

    private final Setting<Boolean> updatePositionFailsafe = sgGeneral.add(new BoolSetting.Builder()
            .name("failsafe")
            .description("Updates your position to the top of the target block if you miss the jump.")
            .defaultValue(true)
            .build()
    );

    private final Setting<Double> failsafeWindow = sgGeneral.add(new DoubleSetting.Builder()
            .name("failsafe-window")
            .description("Window below the target block to fall to trigger failsafe.")
            .min(0.01)
            .max(1)
            .sliderMin(0.01)
            .sliderMax(1)
            .defaultValue(0.10)
            .build()
    );

    private final Setting<Double> successfulLandingMargin = sgGeneral.add(new DoubleSetting.Builder()
            .name("landing-margin")
            .description("The distance from a landing block to be considered a successful landing.")
            .min(0.01)
            .max(10)
            .sliderMin(0.01)
            .sliderMax(10)
            .defaultValue(1)
            .build()
    );

    public TanukiMode() {
        super(Category.Movement, "TanukiMode", "Makes moving on bedrock easier.");
    }

    private final BlockPos.Mutable blockPos = new BlockPos.Mutable(0, 0, 0);
    private final ArrayList<BlockPos> validBlocks = new ArrayList<>();
    private final TreeMap<Double, BlockPos> sortedBlocks = new TreeMap<>();
    private final BlockPos.Mutable playerHorizontalPos = new BlockPos.Mutable();

    private boolean successfulLanding;

    @Override
    public void onActivate() {
        if(searchRadius.get() % 2 == 0) {
            ChatUtils.moduleInfo(this, "%d is not valid for radius, rounding up", searchRadius.get());
            searchRadius.set(searchRadius.get() + 1);
        }
    }

    @EventHandler
    private void onTick(TickEvent.Post event) {

        if(mc.player.getY() > (driftToHeight.get() + activationWindow.get())) return;

        Vec3d targetPos = findNearestBlock(mc.player.getX(), driftToHeight.get() - 1, mc.player.getZ());

        if(targetPos == null) return;

        if(mc.player.getY() == targetPos.getY() + 1) return;

        if(mc.options.keyJump.isPressed()) return;

        if(updatePositionFailsafe.get() && (!successfulLanding)) {
            if(mc.player.getY() < driftToHeight.get() - failsafeWindow.get()) {
                mc.player.updatePosition(targetPos.getX(), targetPos.getY() + 1, targetPos.getZ());
            }
        }

        Vec3d normalizedDirection = targetPos.subtract(mc.player.getPos()).normalize();

        ((IVec3d) mc.player.getVelocity()).set(
                mc.player.getVelocity().x + (normalizedDirection.x * horizontalPullStrength.get() * mc.getTickDelta()),
                mc.player.getVelocity().y + (normalizedDirection.y * verticalPullStrength.get() * mc.getTickDelta()),
                mc.player.getVelocity().z + (normalizedDirection.z * horizontalPullStrength.get() * mc.getTickDelta())
        );

        successfulLanding = mc.player.getPos().isInRange(targetPos, successfulLandingMargin.get());

    }

    private Vec3d findNearestBlock(double x, int y, double z) {
        validBlocks.clear();
        sortedBlocks.clear();

        playerHorizontalPos.set(x, y, z);

        int rad = searchRadius.get() - 1;
        for(int i = 0; i < searchRadius.get(); i++) {
            for(int j = 0; j < searchRadius.get(); j++) {
                BlockState bs = mc.world.getBlockState(blockPos.set(x - (rad / 2 - i), y, z - (rad / 2 - j)));
                if(!bs.isAir() && bs.getBlock() != Blocks.LAVA && bs.getBlock() != Blocks.WATER) {
                    validBlocks.add(blockPos.mutableCopy());
                }
            }
        }

        validBlocks.forEach(blockPos -> {
            sortedBlocks.put(blockPos.getSquaredDistance(x, y, z, true), blockPos);
        });

        Map.Entry<Double, BlockPos> firstEntry = sortedBlocks.firstEntry();

        if(firstEntry == null) {
            return null;
        } else {
            return Vec3d.ofBottomCenter(firstEntry.getValue());
        }
    }

}
