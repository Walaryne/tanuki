package minegame159.meteorclient.commands.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import minegame159.meteorclient.commands.Command;
import net.minecraft.client.MinecraftClient;
import net.minecraft.command.CommandSource;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.network.packet.c2s.play.BookUpdateC2SPacket;

import static com.mojang.brigadier.Command.SINGLE_SUCCESS;

public class PopBobSexDupe extends Command {

    public PopBobSexDupe() {
        super("popbobsexdupe", "lol");
    }

    MinecraftClient mc = MinecraftClient.getInstance();

    @Override
    public void build(LiteralArgumentBuilder<CommandSource> builder) {
        builder.executes(context -> {
            ItemStack itemStack = new ItemStack(Items.WRITABLE_BOOK, 1);
            ListTag pages = new ListTag();
            pages.addTag(0, StringTag.of("DUPE"));
            itemStack.putSubTag("pages", pages);
            itemStack.putSubTag("title", StringTag.of("a"));
            mc.getNetworkHandler().sendPacket(new BookUpdateC2SPacket(itemStack, true, mc.player.inventory.selectedSlot));

            return SINGLE_SUCCESS;
        });
    }
}
