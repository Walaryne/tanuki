package minegame159.meteorclient.mixin.sodium;

import me.jellysquid.mods.sodium.client.model.light.LightPipeline;
import me.jellysquid.mods.sodium.client.model.quad.ModelQuadViewMutable;
import me.jellysquid.mods.sodium.client.render.pipeline.FluidRenderer;
import me.jellysquid.mods.sodium.client.util.color.ColorARGB;
import minegame159.meteorclient.modules.Modules;
import minegame159.meteorclient.modules.render.Ambience;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Arrays;

@Mixin(FluidRenderer.class)
public class SodiumFluidRendererMixin {

    @Final
    @Shadow(remap = false)
    private int[] quadColors;

    @Inject(method = "calculateQuadColors", at = @At("TAIL"), cancellable = true, remap = false)
    private void onCalculateQuadColors(ModelQuadViewMutable quad, BlockRenderView world, BlockPos pos, LightPipeline lighter, Direction dir, float brightness, boolean colorized, CallbackInfo ci) {
        Ambience ambience = Modules.get().get(Ambience.class);
        if(ambience.isActive() && ambience.changeLavaColor() && !colorized) {
            Arrays.fill(quadColors, ColorARGB.toABGR(ambience.getLavaColor()));
        }
    }

}
