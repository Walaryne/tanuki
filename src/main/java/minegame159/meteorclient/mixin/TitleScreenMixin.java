/*
 * This file is part of the Meteor Client distribution (https://github.com/MeteorDevelopment/meteor-client/).
 * Copyright (c) 2020 Meteor Development.
 */

package minegame159.meteorclient.mixin;

import minegame159.meteorclient.utils.Utils;
import minegame159.meteorclient.utils.render.color.Color;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TitleScreen.class)
public class TitleScreenMixin extends Screen {
    private static final Identifier QUBE_ICON = new Identifier("qube-client", "icon.png");

    private int colorClientTitle;
    private int colorGrey;
    private int colorWhite;

    private String text1;
    private int text1Length;

    private String text2;
    private int text2Length;

    private String text3;
    private int text3Length;

    private String text4;
    private int text4Length;

    private String text5;
    private int text5Length;

    public TitleScreenMixin(Text title) {
        super(title);
    }

    @Inject(method = "init", at = @At("TAIL"))
    private void onInit(CallbackInfo info) {
        colorClientTitle = Color.fromRGBA(255, 0, 255, 255);
        colorGrey = Color.fromRGBA(175, 175, 175, 175);
        colorWhite = Color.fromRGBA(255, 255, 255, 255);

        text1 = "Tanuki Client ";
        text2 = "by ";
        text3 = "Walaryne ";
        text4 = "and ";
        text5 = "fluse1367";

        text1Length = textRenderer.getWidth(text1);
        text2Length = textRenderer.getWidth(text2);
        text3Length = textRenderer.getWidth(text3);
        text4Length = textRenderer.getWidth(text4);
        text5Length = textRenderer.getWidth(text5);
    }

    @Inject(method = "render", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/screen/TitleScreen;drawStringWithShadow(Lnet/minecraft/client/util/math/MatrixStack;Lnet/minecraft/client/font/TextRenderer;Ljava/lang/String;III)V", ordinal = 0))
    private void onRenderIdkDude(MatrixStack matrices, int mouseX, int mouseY, float delta, CallbackInfo info) {
        if (Utils.firstTimeTitleScreen) {
            Utils.firstTimeTitleScreen = false;
        }
    }

    @Inject(method = "render", at = @At("TAIL"))
    private void onRender(MatrixStack matrices, int mouseX, int mouseY, float delta, CallbackInfo info) {
        client.getTextureManager().bindTexture(QUBE_ICON);
        drawTexture(matrices, width - 19, 3, 0, 0, 19, 20, 19, 20);

        textRenderer.drawWithShadow(matrices, text1, width - text5Length - text4Length - text3Length - text2Length - text1Length - 19 - 3, 10, colorClientTitle);
        textRenderer.drawWithShadow(matrices, text2, width - text5Length - text4Length - text3Length - text2Length - 19 - 3, 10, colorGrey);
        textRenderer.drawWithShadow(matrices, text3, width - text5Length - text4Length - text3Length - 19 - 3, 10, colorWhite);
        textRenderer.drawWithShadow(matrices, text4, width - text5Length - text4Length - 19 - 3, 10, colorGrey);
        textRenderer.drawWithShadow(matrices, text5, width - text5Length - 19 - 3, 10, colorWhite);
    }
}
