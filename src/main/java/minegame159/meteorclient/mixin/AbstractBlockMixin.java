/*
 * This file is part of the Meteor Client distribution (https://github.com/MeteorDevelopment/meteor-client/).
 * Copyright (c) 2021 Meteor Development.
 */

package minegame159.meteorclient.mixin;

import minegame159.meteorclient.MeteorClient;
import minegame159.meteorclient.events.world.AmbientOcclusionEvent;
import minegame159.meteorclient.events.world.CollisionShapeEvent;
import minegame159.meteorclient.events.world.FluidCollisionShapeEvent;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(AbstractBlock.class)
public class AbstractBlockMixin {
    @Inject(method = "getAmbientOcclusionLightLevel", at = @At("HEAD"), cancellable = true)
    private void onGetAmbientOcclusionLightLevel(BlockState state, BlockView world, BlockPos pos, CallbackInfoReturnable<Float> info) {
        AmbientOcclusionEvent event = MeteorClient.EVENT_BUS.post(AmbientOcclusionEvent.get());

        if (event.lightLevel != -1) info.setReturnValue(event.lightLevel);
    }

    @Inject(method = "getCollisionShape", at = @At("RETURN"), cancellable = true)
    public void onCollisionShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context, CallbackInfoReturnable<VoxelShape> ci) {
        VoxelShape ret = ci.getReturnValue();
        CollisionShapeEvent csevent = MeteorClient.EVENT_BUS.post(CollisionShapeEvent.get(state, world, pos, ret));
        if (csevent.shape != ret) // return altered value if it was changed
            ci.setReturnValue(csevent.shape);

        if (!(state.getFluidState().isEmpty())) {
            FluidCollisionShapeEvent fcsevent = MeteorClient.EVENT_BUS.post(FluidCollisionShapeEvent.get(state.getFluidState().getBlockState()));

            if (fcsevent.shape != null) ci.setReturnValue(fcsevent.shape);
        }
    }
}
