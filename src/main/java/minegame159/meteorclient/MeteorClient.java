/*
 * This file is part of the Meteor Client distribution (https://github.com/MeteorDevelopment/meteor-client/).
 * Copyright (c) 2021 Meteor Development.
 */

package minegame159.meteorclient;

import meteordevelopment.orbit.EventBus;
import meteordevelopment.orbit.EventHandler;
import meteordevelopment.orbit.IEventBus;
import minegame159.meteorclient.events.entity.EntityAddedEvent;
import minegame159.meteorclient.events.game.GameLeftEvent;
import minegame159.meteorclient.events.meteor.ClientInitialisedEvent;
import minegame159.meteorclient.events.meteor.KeyEvent;
import minegame159.meteorclient.events.world.TickEvent;
import minegame159.meteorclient.gui.WidgetScreen;
import minegame159.meteorclient.gui.screens.topbar.TopBarModules;
import minegame159.meteorclient.modules.Modules;
import minegame159.meteorclient.modules.misc.DiscordPresence;
import minegame159.meteorclient.modules.render.hud.HudEditorScreen;
import minegame159.meteorclient.rendering.Fonts;
import minegame159.meteorclient.rendering.Matrices;
import minegame159.meteorclient.rendering.text.CustomTextRenderer;
import minegame159.meteorclient.systems.Systems;
import minegame159.meteorclient.utils.Utils;
import minegame159.meteorclient.utils.entity.EntityUtils;
import minegame159.meteorclient.utils.misc.Names;
import minegame159.meteorclient.utils.misc.input.KeyAction;
import minegame159.meteorclient.utils.misc.input.KeyBinds;
import minegame159.meteorclient.utils.network.Capes;
import minegame159.meteorclient.utils.network.MeteorExecutor;
import minegame159.meteorclient.utils.network.OnlinePlayers;
import minegame159.meteorclient.utils.player.EChestMemory;
import minegame159.meteorclient.utils.player.Rotations;
import minegame159.meteorclient.utils.render.color.RainbowColors;
import minegame159.meteorclient.utils.world.BlockIterator;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.options.KeyBinding;
import net.minecraft.client.options.Perspective;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LightningEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particle.DustParticleEffect;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.*;

public class MeteorClient implements ClientModInitializer {
    public static MeteorClient INSTANCE;
    public static final IEventBus EVENT_BUS = new EventBus();
    public static final File FOLDER = new File(FabricLoader.getInstance().getGameDir().toString(), "meteor-client");
    public static final Logger LOG = LogManager.getLogger();

    public HashMap<UUID, ParticleEffect> particleUUIDMap;

    private int ticks = 0;

    public ArrayList<UUID> lightningUUIDs;

    private boolean lightningOnceFlag = false;

    private Random random;

    public static CustomTextRenderer FONT;

    private MinecraftClient mc;

    public Screen screenToOpen;

    @Override
    public void onInitializeClient() {
        if (INSTANCE == null) {
            KeyBinds.Register();

            INSTANCE = this;
            return;
        }

        System.out.println("Initializing Tanuki Client.");

        mc = MinecraftClient.getInstance();
        Utils.mc = mc;
        EntityUtils.mc = mc;

        Systems.addPreLoadTask(() -> {
            if (!Modules.get().getFile().exists()) {
                Modules.get().get(DiscordPresence.class).toggle(false);
            }
        });

        Matrices.begin(new MatrixStack());
        Fonts.init();
        MeteorExecutor.init();
        Capes.init();
        RainbowColors.init();
        BlockIterator.init();
        EChestMemory.init();
        Rotations.init();
        Names.init();

        Systems.init();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Systems.save();
            OnlinePlayers.leave();
        }));

        registerParticles();

        EVENT_BUS.subscribe(this);
        EVENT_BUS.post(new ClientInitialisedEvent());
    }

    private void openClickGui() {
        mc.openScreen(new TopBarModules());
    }

    @EventHandler
    private void onGameLeft(GameLeftEvent event) {
        Systems.save();
    }

    @EventHandler
    private void onTick(TickEvent.Post event) {
        ticks++;

        Capes.tick();

        if (screenToOpen != null && mc.currentScreen == null) {
            mc.openScreen(screenToOpen);
            screenToOpen = null;
        }

        if (Utils.canUpdate()) {
            mc.player.getActiveStatusEffects().values().removeIf(statusEffectInstance -> statusEffectInstance.getDuration() <= 0);
        }

        if(mc.world != null && mc.player != null) {
            for(Entity entity : mc.world.getEntities()) {
                if(entity instanceof PlayerEntity) {
                    PlayerEntity player = (PlayerEntity) entity;
                    for(Map.Entry<UUID, ParticleEffect> entry : particleUUIDMap.entrySet()) {
                        if(entry.getKey().equals(player.getUuid())) {
                            renderPlayerParticles(entry.getKey(), entry.getValue(), player);
                        }
                    }
                }
            }
        }

        if(ticks == 20) {
            ticks = 0;
        }
    }

    private void registerParticles() {
        random = new Random();

        particleUUIDMap = new HashMap<>();
        lightningUUIDs = new ArrayList<>();

        //Walaryne
        lightningUUIDs.add(UUID.fromString("15cbc8bf-0750-41b5-8a30-94fb5e47e41d"));
        //TanukiLuki
        lightningUUIDs.add(UUID.fromString("2ff2ceca-ce9a-4c30-9f1c-8d6966438cbb"));
        //fluse1367
        lightningUUIDs.add(UUID.fromString("dc470a6a-6e22-4ec6-b249-068126dc9deb"));

        //Walaryne
        particleUUIDMap.put(UUID.fromString("15cbc8bf-0750-41b5-8a30-94fb5e47e41d"), ParticleTypes.PORTAL);
        //TanukiLuki
        particleUUIDMap.put(UUID.fromString("2ff2ceca-ce9a-4c30-9f1c-8d6966438cbb"), ParticleTypes.PORTAL);
        //fluse1367
        particleUUIDMap.put(UUID.fromString("dc470a6a-6e22-4ec6-b249-068126dc9deb"), ParticleTypes.PORTAL);
        //wornscarf
        particleUUIDMap.put(UUID.fromString("e04e00f7-19ad-4fcf-a90d-6b17600fcce8"), DustParticleEffect.RED);
        //JakeOrgen
        particleUUIDMap.put(UUID.fromString("f02edc1a-6dbb-4353-b253-078ffc7feffb"), DustParticleEffect.RED);
        //Amizziko
        particleUUIDMap.put(UUID.fromString("bca478bd-f514-4986-bdd7-640ac002527b"), ParticleTypes.SOUL);
        //AncyenT
        particleUUIDMap.put(UUID.fromString("cd5c0544-6993-4dfa-a086-c9320595e49e"), ParticleTypes.SOUL);
        //Spartnt
        particleUUIDMap.put(UUID.fromString("875bae2a-8eae-40aa-8912-a07ee6c9da29"), ParticleTypes.TOTEM_OF_UNDYING);
        //GAM3R
        particleUUIDMap.put(UUID.fromString("6dbaa9a1-4e96-4458-ac07-036269e7d389"), ParticleTypes.HEART);
        //moshpitmukey
        particleUUIDMap.put(UUID.fromString("ffdd15d8-3423-4c18-a32b-af93e0d5131d"), ParticleTypes.HEART);
        //SpicyRedPotato
        particleUUIDMap.put(UUID.fromString("090c043f-0762-45db-8f84-6c1a1a5fd842"), ParticleTypes.BUBBLE);
        //cqb13
        particleUUIDMap.put(UUID.fromString("408fb01f-3ac5-4fa7-aa65-9fba051c9c51"), ParticleTypes.NAUTILUS);
        //IcatIcatI
        particleUUIDMap.put(UUID.fromString("ff8a62c2-b2d7-4334-8794-e0af3b9ad8c2"), ParticleTypes.NAUTILUS);
        //D10
        particleUUIDMap.put(UUID.fromString("bc48b56d-d2e2-4838-ae6d-bd26559c1267"), ParticleTypes.COMPOSTER);
        //LVOVSU
        particleUUIDMap.put(UUID.fromString("2eac801f-d1b6-4c01-8db1-3de2f3f578b0"), ParticleTypes.SOUL_FIRE_FLAME);
        //DustyOsis
        particleUUIDMap.put(UUID.fromString("ade122a9-bce3-408a-b509-97d65c499538"), ParticleTypes.FALLING_NECTAR);
    }

    private void renderPlayerParticles(UUID uuid, ParticleEffect effect, PlayerEntity player) {
        if (!(uuid.equals(mc.player.getUuid()) && mc.options.getPerspective() == Perspective.FIRST_PERSON)) {
            if(effect == ParticleTypes.SOUL) {
                mc.world.addParticle(ParticleTypes.SOUL, player.getX() + (random.nextDouble() - 0.5D) * (double)player.getWidth(), player.getY() + 0.1D, player.getZ() + (random.nextDouble() - 0.5D) * (double)player.getWidth(), player.getVelocity().x * -0.2D, 0.1D, player.getVelocity().z * -0.2D);
            } else if(effect == ParticleTypes.HEART) {
                if((ticks % 2) == 0) {
                    double d = random.nextGaussian() * 0.02D;
                    double e = random.nextGaussian() * 0.02D;
                    double f = random.nextGaussian() * 0.02D;
                    mc.world.addParticle(ParticleTypes.HEART, player.getParticleX(1.0D), player.getRandomBodyY() + 0.5D, player.getParticleZ(1.0D), d, e, f);
                }
            } else {
                for (int i = 0; i < 2; ++i) {
                    mc.world.addParticle(effect, player.getParticleX(0.5D), player.getRandomBodyY() - 0.25D, player.getParticleZ(0.5D), (random.nextDouble() - 0.5D) * 2.0D, -random.nextDouble(), (random.nextDouble() - 0.5D) * 2.0D);
                }
            }
        }
    }

    @EventHandler
    private void onKey(KeyEvent event) {
        // Click GUI
        if (event.action == KeyAction.Press && event.key == KeyBindingHelper.getBoundKeyOf(KeyBinds.OPEN_CLICK_GUI).getCode()) {
            if ((!Utils.canUpdate() && !(mc.currentScreen instanceof WidgetScreen) && !(mc.currentScreen instanceof HudEditorScreen)) || mc.currentScreen == null) openClickGui();
        }

        // Shulker Peek
        KeyBinding shulkerPeek = KeyBinds.SHULKER_PEEK;
        shulkerPeek.setPressed(shulkerPeek.matchesKey(event.key, 0) && event.action != KeyAction.Release);
    }

    @EventHandler
    private void onUwu(EntityAddedEvent event) {
        if(lightningUUIDs.contains(event.entity.getUuid())) {

            double x = event.entity.getX();
            double y = event.entity.getY();
            double z = event.entity.getZ();

            LightningEntity lightningEntity = new LightningEntity(EntityType.LIGHTNING_BOLT, mc.world);

            lightningEntity.updatePosition(x, y, z);
            lightningEntity.refreshPositionAfterTeleport(x, y, z);

            mc.world.addEntity(lightningEntity.getEntityId(), lightningEntity);

            if(!lightningOnceFlag) {
                mc.world.playSound(mc.player, x, y, z, SoundEvents.ENTITY_LIGHTNING_BOLT_THUNDER, SoundCategory.WEATHER, 10000.0F, 0.8F * 0.2F);
                mc.world.playSound(mc.player, x, y, z, SoundEvents.ENTITY_LIGHTNING_BOLT_IMPACT, SoundCategory.WEATHER, 2.0F, 0.5F * 0.2F);
                lightningOnceFlag = true;
            }
        }
    }
}
