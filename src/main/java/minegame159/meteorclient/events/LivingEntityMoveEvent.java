package minegame159.meteorclient.events;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.Vec3d;

public class LivingEntityMoveEvent {
    public LivingEntity entity;
    public Vec3d movement;
}
