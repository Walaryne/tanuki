package minegame159.meteorclient.events.world;

import minegame159.meteorclient.events.Cancellable;
import net.minecraft.util.math.Vec3d;

public class GetCloudColorEvent extends Cancellable {
    private static final GetCloudColorEvent INSTANCE = new GetCloudColorEvent();

    public Vec3d color;
    public float tickDelta;

    public static GetCloudColorEvent get(float tickDelta) {
        INSTANCE.setCancelled(false);
        INSTANCE.color = null;
        INSTANCE.tickDelta = tickDelta;
        return INSTANCE;
    }
}
