package minegame159.meteorclient.events.world;

import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;

public class CollisionShapeEvent {
    public static final CollisionShapeEvent INSTANCE = new CollisionShapeEvent();

    public BlockState state;
    public BlockView world;
    public BlockPos pos;
    public VoxelShape shape;

    public static CollisionShapeEvent get(BlockState state, BlockView world, BlockPos pos, VoxelShape shape) {
        INSTANCE.state = state;
        INSTANCE.world = world;
        INSTANCE.pos = pos;
        INSTANCE.shape = shape;
        return INSTANCE;
    }
}
