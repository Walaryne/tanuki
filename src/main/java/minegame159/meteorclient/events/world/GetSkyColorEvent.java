package minegame159.meteorclient.events.world;

import minegame159.meteorclient.events.Cancellable;
import net.minecraft.util.math.Vec3d;

public class GetSkyColorEvent extends Cancellable {
    private static final GetSkyColorEvent INSTANCE = new GetSkyColorEvent();

    public Vec3d color;
    public float tickDelta;

    public static GetSkyColorEvent get(float tickDelta) {
        INSTANCE.setCancelled(false);
        INSTANCE.color = null;
        INSTANCE.tickDelta = tickDelta;
        return INSTANCE;
    }

}
