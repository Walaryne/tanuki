package minegame159.meteorclient.events.entity.player;

import minegame159.meteorclient.events.Cancellable;

public class DropItemEvent extends Cancellable {
    private static final DropItemEvent INSTANCE = new DropItemEvent();

    public static DropItemEvent get() {
        INSTANCE.setCancelled(false);

        return INSTANCE;
    }

}
